import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

const apiUrl = "http://localhost:8080";

@Injectable()
export class HttpConfigProvider {
  constructor(public http: HttpClient) {
    console.log("Hello HttpConfigProvider Provider");
  }

  private getHttpOptions() {
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    headers.append("Access-Control-Allow-Origin", "http://localhost:8080/");
    headers.append(
      "Access-Control-Allow-Methods",
      "POST, GET, OPTIONS, PUT, DELETE"
    );
    headers.append(
      "Access-Control-Allow-Headers",
      "X-Requested-With, Content-Type, Authorization, Origin, Accept"
    );
    headers.append("Access-Control-Allow-Credentials", "true");
    headers.append("withCredentials", "true");
    headers.append("Accept", "application/json");
    return {
      headers: headers
    };
  }

  public get(path): Observable<any> {
    const url = `${apiUrl}/${path}`;
    return this.http.get(url, this.getHttpOptions()).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  public delete(path): Observable<any> {
    const url = `${apiUrl}/${path}`;
    return this.http.delete(url, this.getHttpOptions()).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  public post(path, data): Observable<any> {
    const url = `${apiUrl}/${path}`;
    console.log(url);
    return this.http
      .post(url, data, this.getHttpOptions())
      .pipe(catchError(this.handleError));
  }

  public put(path, data): Observable<any> {
    const url = `${apiUrl}/${path}`;
    console.log(url);
    return this.http
      .put(url, data, this.getHttpOptions())
      .pipe(catchError(this.handleError));
  }

  postData(formData: FormData) {
    const urlUpload = `${apiUrl}/api/upload`;
    this.http
      .post<string>(urlUpload, formData)
      .pipe(catchError(e => this.handleError(e)))
      .subscribe(ok => console.log(ok));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    console.log(error);
    return Observable.throw("Something bad happened; please try again later.");
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }
}
