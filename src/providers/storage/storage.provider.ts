import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";

@Injectable()
export class StorageProvider {
  constructor(public storage: Storage) {}
  /**
   * @method Promise<any> responsável por recuperar um item no storage
   * @param key string
   */
  getStorage(key: string): Promise<any> {
    return this.storage.get(key);
  }
  /**
   * @method Promise<any> responsável por setar um item no storage
   * @param key type string
   * @param data type any
   */
  setStorage(key: string, data): Promise<any> {
    return this.storage.set(key, data);
  }
  /**
   * @method Promise<any> responsável por remover um item no storage
   * @param key type string
   */
  removeStorage(key: string): Promise<any> {
    return this.storage.remove(key);
  }
}
