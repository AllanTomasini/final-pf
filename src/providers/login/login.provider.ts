import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Subject } from "rxjs/Subject";
import { AuthModel } from "../../models/AuthModel";
import { AutenticarUsuarioModel } from "../../models/AutenticarUsuarioModel";
import { ServerError } from "../../models/ServerError";
import { HttpConfigProvider } from "../http-config/http-config";

@Injectable()
export class LoginProvider {
  private subUsuario: ReplaySubject<any> = new ReplaySubject<any>(1);
  private unsubscribe = new Subject<void>();
  constructor(
    public httpConfig: HttpConfigProvider,
    private http: HttpClient
  ) {}

  /**
   * @method Observable método responsável por retornar os dados do usuário da memória
   * @returns Retorna uma observable com os dados do usuário
   */

  getSubUsuario(): Observable<AuthModel> {
    return this.subUsuario.asObservable().takeUntil(this.unsubscribe);
  }

  /**
   * @method Observable método responsável por remover os dados do usuário da memória
   */

  removeSubUsuario(): void {
    this.unsubscribe.next();
  }

  /**
   * @param data recebe os dados de autenticação do usuário
   */
  setSubUsuario(data: AuthModel): void {
    this.subUsuario.next(data);
  }

  /**
   * @method Observable método responsável por fazer login do usuário e salvar na memória
   * @returns Retorna uma Observable dos dados do usuário
   */

  doLogin(
    dataUser,
    mock = true
  ): Observable<AutenticarUsuarioModel | ServerError> {
    if (mock) {
      return this.http.get<AutenticarUsuarioModel>(
        "assets/data/login-mock.json"
      );
    }
  }
}
