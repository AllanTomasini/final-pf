import { Injectable } from '@angular/core';
import {HttpConfigProvider} from "../http-config/http-config";
import {Observable} from "rxjs";
import {Evento} from "../../models/evento";

/*
  Generated class for the EventoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const path = "eventos";
@Injectable()
export class EventoProvider {
  constructor(public httpConfig: HttpConfigProvider) {
    console.log('Hello SessaoProvider Provider');
  }

  public adicionar(body): Observable<any> {
    return this.httpConfig.post(`${path}/inserir`, body);
  }

  public listar(): Observable<Evento[]> {
    return this.httpConfig.get(`${path}/listar`);
  }

  public findById(id: string) : Observable<Evento>{
    return this.httpConfig.get(`${path}/${id}`);
  }
}
