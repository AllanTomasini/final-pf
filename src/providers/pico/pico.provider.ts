import { Denuncia } from "./../../models/denuncia";
import { Feed } from "./../../models/feed";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Pico } from "../../models/pico";
import { HttpConfigProvider } from "../http-config/http-config";
import { Video } from "../../models/video";
import { Foto } from "../../models/foto";

const path = "picos";

@Injectable()
export class PicoProvider {
  constructor(public httpConfig: HttpConfigProvider) {
    console.log("Hello PicoProvider Provider");
  }

  public adicionar(body): Observable<Pico> {
    return this.httpConfig.post(`${path}/inserir`, body);
  }

  public adicionarVideo(body): Observable<Video> {
    return this.httpConfig.post(`${path}/inserirVideo`, body);
  }
  public adicionarFoto(body): Observable<Foto> {
    return this.httpConfig.post(`${path}/inserirFoto`, body);
  }
  public adicionarDenuncia(body): Observable<Denuncia> {
    return this.httpConfig.post(`denuncias/inserir`, body);
  }
  public rejeitarDenuncia(body): Observable<Denuncia> {
    return this.httpConfig.post(`denuncias/rejeitar`, body);
  }
  public aceitarDenuncia(body): Observable<Denuncia> {
    return this.httpConfig.post(`denuncias/aceitar`, body);
  }

  public listarDenuncias(): Observable<Denuncia[]> {
    return this.httpConfig.get(`denuncias/listar`);
  }

  public listar(): Observable<Pico[]> {
    return this.httpConfig.get(`${path}/listar`);
  }
  public listarFeed(): Observable<Feed[]> {
    return this.httpConfig.get(`feed/listar`);
  }
  public listarFavoritos(): Observable<Pico[]> {
    return this.httpConfig.get(`${path}/listarFavoritos`);
  }

  public findById(id: string): Observable<Pico> {
    return this.httpConfig.get(`${path}/${id}`);
  }

  public editarFavorito(id: string): Observable<void> {
    return this.httpConfig.get(`${path}/${id}/editarFavorito`);
  }

  public comentar(id: string, body): Observable<void> {
    return this.httpConfig.post(`${path}/${id}/comentar`, body);
  }
}
