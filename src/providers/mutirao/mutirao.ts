import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Mutirao } from "../../models/mutirao";
import { Usuario } from "../../models/usuario";
import { HttpConfigProvider } from "../http-config/http-config";

/*
  Generated class for the MutiraoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const path = "mutiroes";
@Injectable()
export class MutiraoProvider {
  constructor(public httpConfig: HttpConfigProvider) {
    console.log("Hello MutiraoProvider Provider");
  }
  public adicionar(body): Observable<Mutirao> {
    return this.httpConfig.post(`${path}/inserir`, body);
  }

  public listar(): Observable<Mutirao[]> {
    return this.httpConfig.get(`${path}/listar`);
  }
  public deleteById(id: string): Observable<Mutirao> {
    return this.httpConfig.delete(`${path}/${id}/excluir`);
  }

  public editar(body, id: string): Observable<Mutirao> {
    return this.httpConfig.put(`${path}/${id}/editar`, body);
  }

  public findById(id: string): Observable<Mutirao> {
    return this.httpConfig.get(`${path}/${id}`);
  }

  public listarUsuarios(): Observable<Usuario[]> {
    return this.httpConfig.get(`${path}/listarUsuarios`);
  }
  public participar(id: string): Observable<void> {
    return this.httpConfig.get(`${path}/${id}/participar`);
  }
  public sair(id: string): Observable<void> {
    return this.httpConfig.get(`${path}/${id}/sair`);
  }
}
