import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpConfigProvider} from "../http-config/http-config";
import {Anuncio} from "../../models/anuncio";


const path = "anuncios";
@Injectable()

export class AnuncioProvider {

  constructor(public httpConfig: HttpConfigProvider) {
    console.log('Hello AnuncioProvider Provider');
  }
  public adicionar(body): Observable<Anuncio> {
    return this.httpConfig.post(`${path}/inserir`, body);
  }

  public listar(): Observable<Anuncio[]> {
    return this.httpConfig.get(`${path}/listar`);
  }

  public deleteById(id: string) : Observable<Anuncio> {
    return this.httpConfig.delete(`${path}/${id}/excluir`);
  }

  public editar(body, id : string): Observable<Anuncio> {
    return this.httpConfig.put(`${path}/${id}/editar`, body);
  }

  public findById(anuncio_id: string) : Observable<Anuncio>{
    return this.httpConfig.get(`${path}/${anuncio_id}`);
  }
}
