import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {HttpConfigProvider} from "../http-config/http-config";
import {Observable} from "rxjs";
import {Favorito} from "../../models/favorito";

/*
  Generated class for the FavoritoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const path = "favoritoes";
@Injectable()
export class FavoritoProvider {
  constructor(public httpConfig: HttpConfigProvider) {
    console.log('Hello MutiraoProvider Provider');
  }
  public adicionar(body): Observable<any> {
    return this.httpConfig.post(`${path}/inserir`, body);
  }

  public listar(): Observable<Favorito[]> {
    return this.httpConfig.get(`${path}/listar`);
  }


}
