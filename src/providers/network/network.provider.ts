import { Injectable } from "@angular/core";
import { Network } from "@ionic-native/network";
import { Events } from "ionic-angular";

@Injectable()
export class NetworkProvider {
  private previousStatus;
  constructor(private network: Network, private eventCtrl: Events) {
    this.previousStatus = ConnectionStatusEnum.Online;
  }

  initNetWork(): void {
    this.network.onDisconnect().subscribe(() => {
      if (this.previousStatus === ConnectionStatusEnum.Online) {
        this.eventCtrl.publish("network:offline");
      }
      this.previousStatus = ConnectionStatusEnum.Offline;
    });
    this.network.onConnect().subscribe(() => {
      if (this.previousStatus === ConnectionStatusEnum.Offline) {
        this.eventCtrl.publish("network:online");
      }
      this.previousStatus = ConnectionStatusEnum.Online;
    });
  }

  isConnected(): boolean {
    return this.previousStatus === ConnectionStatusEnum.Online;
  }
}
export enum ConnectionStatusEnum {
  Online,
  Offline
}
