import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpConfigProvider} from "../http-config/http-config";
import {Sessao} from "../../models/sessao";




const path = "sessaos";
@Injectable()
export class SessaoProvider {

  constructor(public httpConfig: HttpConfigProvider) {
    console.log('Hello SessaoProvider Provider');
  }

  public adicionar(body): Observable<Sessao> {
    return this.httpConfig.post(`${path}/inserir`, body);
  }

  public listar(): Observable<any> {
    return this.httpConfig.get(`${path}/listar`);
  }
  public findById(id: string) : Observable<Sessao>{
    return this.httpConfig.get(`${path}/${id}`);
  }
  public deleteById(id: string) : Observable<Sessao> {
    return this.httpConfig.delete(`${path}/${id}/excluir`);
  }

  public editar(body, id : string): Observable<Sessao> {
    console.log(body);
      return this.httpConfig.put(`${path}/${id}/editar`, body);
  }
}
