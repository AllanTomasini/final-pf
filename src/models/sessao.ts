import {Pico} from "./pico";

export interface Sessao {

  pico: Pico;
  dataSessao: Date;
  dataCadastro: Date;

}
