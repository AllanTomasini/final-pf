import { Usuario } from "./usuario";

export class Foto {
  id: string;
  path: string;
  descricao: string;
  usuario: Usuario;
}
