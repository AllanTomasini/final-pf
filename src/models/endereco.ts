import { Localizacao } from './localizacao';

export class Endereco {
  rua: string;
  numero: number;
  localizacao: Localizacao = new Localizacao();
}
