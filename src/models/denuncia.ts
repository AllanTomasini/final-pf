import { Pico } from "./pico";

export class Denuncia {
  id: number;
  data: Date;
  status: string;
  descricao: string;
  pico: Pico;
  idPico: number;
}
