import { Foto } from './foto';
import { Video } from './video';
import { Comentario } from './comentario';
import { Endereco } from "./endereco";

export class Pico {
  id: number;
  urlCapa: string;
  nome: string;
  descricao: string;
  entrada: number;
  dataCriacao: Date;
  endereco: Endereco = new Endereco();
  distancia: number;
  favorito: boolean;
  comentarios: Comentario[] ;
  videos: Video[];
  fotos: Foto[];

}
