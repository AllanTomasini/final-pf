import { Usuario } from "./usuario";

export class Video {
  id: string;
  urlYoutube: string;
  descricao: string;
  usuario: Usuario;
}
