export class ServerError {
  private code: number;
  private message: string;

  constructor(code: number, message: string) {
    this.code = code;
    this.message = message;
  }

  getCode() {
    return this.code;
  }

  getMessage() {
    return this.code >= 500
      ? `${this.message} [COD${this.code}]`
      : `${this.message}`;
  }
}
