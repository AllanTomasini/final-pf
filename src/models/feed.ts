import { Usuario } from './usuario';
export class Feed {
  tipo: string;
  descricao: string;
  usuario: Usuario = new Usuario();
  data: string;
  id: number;
}
