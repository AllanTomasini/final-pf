import { Usuario } from "./usuario";

export class Comentario {
  texto: string;
  id: number;
  usuario: Usuario;
}
