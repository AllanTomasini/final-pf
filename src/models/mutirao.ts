import { Pico } from "./pico";
import { Usuario } from "./usuario";

export class Mutirao {
  id: number;
  dataMutirao: Date;
  participantes: Usuario[];
  criador: Usuario;
  pico: Pico;
}
