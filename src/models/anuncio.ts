export  interface Anuncio {
  titulo : string;
  preco : number;
  descricao : string;
  dataCadastro : Date;
  percentual: number;
  precoComPercentual: number;
}
