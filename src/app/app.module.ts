import { HttpClientModule } from "@angular/common/http";
import { ErrorHandler, LOCALE_ID, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { Camera } from "@ionic-native/camera";
import { File } from "@ionic-native/file";
import { Geolocation } from "@ionic-native/geolocation";
import { LaunchNavigator } from "@ionic-native/launch-navigator";
import { NativeStorage } from "@ionic-native/native-storage";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicStorageModule } from "@ionic/storage";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SideMenuComponent } from "../components/side-menu/side-menu";
import { AnuncioProvider } from "../providers/anuncio/anuncio";
import { AuthProvider } from "../providers/auth/auth.provider";
import { EventoProvider } from "../providers/evento/evento";
import { FavoritoProvider } from "../providers/favorito/favorito";
import { HttpConfigProvider } from "../providers/http-config/http-config";
import { LoginProvider } from "../providers/login/login.provider";
import { MutiraoProvider } from "../providers/mutirao/mutirao";
import { NetworkProvider } from "../providers/network/network.provider";
import { PicoProvider } from "../providers/pico/pico.provider";
import { SessaoProvider } from "../providers/sessao/sessao";
import { StorageProvider } from "../providers/storage/storage.provider";
import { MyApp } from "./app.component";
import { registerLocaleData } from "@angular/common";

import localePt from "@angular/common/locales/pt";

registerLocaleData(localePt, "pt-BR");

@NgModule({
  declarations: [MyApp, SideMenuComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    StatusBar,
    SplashScreen,
    LaunchNavigator,
    // Platform,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: "pt-BR" },
    PicoProvider,
    HttpConfigProvider,
    Geolocation,
    Camera,
    File,
    StorageProvider,
    NetworkProvider,
    LoginProvider,
    NativeStorage,
    AuthProvider,
    SessaoProvider,
    SessaoProvider,
    AnuncioProvider,
    PicoProvider,
    MutiraoProvider,
    EventoProvider,
    FavoritoProvider
  ]
})
export class AppModule {}
