import { Component, ViewChild } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen";
import { MenuController, Nav, Platform } from "ionic-angular";
import { AuthProvider } from "../providers/auth/auth.provider";
import { LoginProvider } from "../providers/login/login.provider";
import { StorageProvider } from "../providers/storage/storage.provider";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav)
  protected nav: Nav;
  protected rootPage: any;

  constructor(
    platform: Platform,
    public splashScreen: SplashScreen,
    private storageProvider: StorageProvider,
    private loginProvider: LoginProvider,
    public menuCtrl: MenuController,
    private authProvider: AuthProvider
  ) {
    platform.ready().then(() => {
      menuCtrl.enable(false);
      this.authProvider.authenticationState.subscribe(state => {
        if (state) {
          this.rootPage = "ListarPicosPage";
          this.menuCtrl.enable(true);
        } else {
          this.rootPage = "LoginPage";
          this.menuCtrl.enable(false);
        }
      });

      splashScreen.hide();
    });
  }
  private hideSplashScreen(): void {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 500);
    }
  }
}
