import { Component, Input } from "@angular/core";
import { Nav, MenuController } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth.provider";

/**
 * Generated class for the SideMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "side-menu",
  templateUrl: "side-menu.html"
})
export class SideMenuComponent {
  @Input()
  protected nav: Nav;

  text: string;

  constructor(
    private menuCtrl: MenuController,
    private authProvider: AuthProvider
  ) {}

  onLoad(page: string) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  logout() {
    this.authProvider.logout();
  }
}
