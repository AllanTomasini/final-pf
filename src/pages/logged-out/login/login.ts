import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AutenticarUsuarioModel } from "../../../models/AutenticarUsuarioModel";
import { LoginProvider } from "../../../providers/login/login.provider";
import { StorageProvider } from "../../../providers/storage/storage.provider";
import { AuthProvider } from "../../../providers/auth/auth.provider";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  protected formLogin: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private loginProvider: LoginProvider,
    private authProvider: AuthProvider
  ) {}

  ngOnInit() {
    console.log("ionViewDidLoad LoginPage");
    this.initFormLogin();
  }

  private initFormLogin(): void {
    this.formLogin = this.formBuilder.group({
      login: ["", [Validators.required]],
      password: ["", Validators.required]
    });
  }

  protected doLogin(): void {
    const dataUser = {
      password: this.formLogin.get("password").value,
      login: this.formLogin.get("login").value.replace(/[\.\-\/]/g, "")
    };

    this.authProvider.login();
  }
}
