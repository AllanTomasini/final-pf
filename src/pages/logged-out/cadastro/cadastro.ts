import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {NgForm} from "@angular/forms";

/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

   constructor(private  loadingCtrl: LoadingController
    , private alertCtrl : AlertController
   ) {
   }
  //
   onSignup(form: NgForm) {
     const loading = this.loadingCtrl.create({
      content: 'Sign you up..'
    });
     loading.present();
    // this.authService.signup(form.value.email, form.value.password)
    //   .then(data => {
    //     loading.dismiss()}
    //   )
    //   .catch(error =>{
    //     loading.dismiss();
    //     const alert = this.alertCtrl.create({
    //       title: 'Signup failed',
    //       message: error.message,
    //       buttons: ['Ok']
    //     });
    //     alert.present();
    //   } );
   }
}
