import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarDenunciasAdminPage } from './listar-denuncias-admin';

@NgModule({
  declarations: [
    ListarDenunciasAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarDenunciasAdminPage),
  ],
})
export class ListarDenunciasAdminPageModule {}
