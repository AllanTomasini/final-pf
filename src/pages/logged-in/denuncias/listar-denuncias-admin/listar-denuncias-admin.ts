import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";
import { Denuncia } from "../../../../models/denuncia";

@IonicPage()
@Component({
  selector: "page-listar-denuncias-admin",
  templateUrl: "listar-denuncias-admin.html"
})
export class ListarDenunciasAdminPage {
  denuncias: Denuncia[];
  loader: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public picoProvider: PicoProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad ListarDenunciasPage");
    this.loadData();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  loadData() {
    this.loader = this.presentLoading();
    this.picoProvider
      .listarDenuncias()
      .toPromise()
      .then(response => {
        console.log(response);
        this.denuncias = response;
      })
      .then(() => this.loader.dismiss())
      .catch(error => {
        console.error(error);
        this.loader.dismiss();
      });
  }

  aceitar(denuncia: Denuncia) {
    this.picoProvider
      .aceitarDenuncia(denuncia)
      .toPromise()
      .then(_response => {
        console.log(_response);
        this.loadData();
      })
      .then(() => this.loader.dismiss())
      .catch(error => {
        console.error(error);
        this.loader.dismiss();
        this.loadData();
      });
  }

  rejeitar(denuncia: Denuncia) {
    this.picoProvider
      .rejeitarDenuncia(denuncia)
      .toPromise()
      .then(_response => {
        this.loadData();
        console.log(_response);
      })
      .then(() => this.loader.dismiss())
      .catch(error => {
        this.loadData();
        console.error(error);
        this.loader.dismiss();
      });
  }
}
