import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarDenunciaPage } from './adicionar-denuncia';

@NgModule({
  declarations: [
    AdicionarDenunciaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarDenunciaPage),
  ],
})
export class AdicionarDenunciaPageModule {}
