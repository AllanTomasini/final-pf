import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";

@IonicPage()
@Component({
  selector: "page-adicionar-denuncia",
  templateUrl: "adicionar-denuncia.html"
})
export class AdicionarDenunciaPage implements OnInit {
  formDenuncia: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public picoProvider: PicoProvider
  ) {}

  ngOnInit() {
    console.log("ionViewDidLoad AdicionarDenunciaPage");
    this.initForm();
  }

  private initForm() {
    this.formDenuncia = this.formBuilder.group({
      descricao: ["", Validators.required]
    });
  }

  public adicionar() {
    let formValues = this.formDenuncia.getRawValue();
    let idPico = this.navParams.get("id");
    console.log(formValues);

    let { descricao } = formValues;
    let body = {
      descricao,
    status: "Aguardando Avaliação",
      idPico: idPico
    };

    console.log("body", body);
    this.picoProvider
      .adicionarDenuncia(body)
      .toPromise()
      .then(
        test => console.log(test),
        error => {
          console.log(error);
        }
      );

    this.navCtrl.pop();
  }
}
