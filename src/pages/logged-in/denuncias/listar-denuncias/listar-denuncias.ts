import { Denuncia } from "./../../../../models/denuncia";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";

/**
 * Generated class for the ListarDenunciasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-listar-denuncias",
  templateUrl: "listar-denuncias.html"
})
export class ListarDenunciasPage {
  denuncias: Denuncia[];
  loader: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public picoProvider: PicoProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad ListarDenunciasPage");
    this.loadData();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  loadData() {
    this.loader = this.presentLoading();
    this.picoProvider
      .listarDenuncias()
      .toPromise()
      .then(response => {
        console.log(response);
        this.denuncias = response;
      })
      .then(() => this.loader.dismiss())
      .catch(error => {
        console.error(error);
        this.loader.dismiss();
      });
  }
}
