import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesquisarContatoPage } from './pesquisar-contato';

@NgModule({
  declarations: [
    PesquisarContatoPage,
  ],
  imports: [
    IonicPageModule.forChild(PesquisarContatoPage),
  ],
})
export class PesquisarContatoPageModule {}
