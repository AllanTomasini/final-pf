import { Component } from "@angular/core";
import { IonicPage, LoadingController, NavController, NavParams } from "ionic-angular";
import { Feed } from "./../../../../models/feed";
import { PicoProvider } from "./../../../../providers/pico/pico.provider";

@IonicPage()
@Component({
  selector: "page-listar-feed",
  templateUrl: "listar-feed.html"
})
export class ListarFeedPage {
  feeds: Feed[];
  loader: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public picoProvider: PicoProvider,
    public loadingCtrl: LoadingController
  ) {}

  ionViewWillEnter() {
    console.log("ionViewDidLoad ListarFeedPage");
    this.loadData();
  }

  loadData() {
    this.loader = this.presentLoading();
    this.picoProvider
      .listarFeed()
      .toPromise()
      .then(response => {
        console.log(response);
        this.feeds = response;
        this.loader.dismiss();
      });
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }
  goToDetalhe(id: number, tipo: string) {
    if (tipo === "Mutirão") {
      this.navCtrl.push("DetalhesMutiraoPage", { id: id });
    }
    if (tipo === "Pico") {
      this.navCtrl.push("DetalhePicoPage", { id: id });
    }
  }
}
