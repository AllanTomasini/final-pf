import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarFeedPage } from './listar-feed';

@NgModule({
  declarations: [
    ListarFeedPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarFeedPage),
  ],
})
export class ListarFeedPageModule {}
