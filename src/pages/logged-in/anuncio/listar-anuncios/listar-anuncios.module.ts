import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarAnunciosPage } from './listar-anuncios';

@NgModule({
  declarations: [
    ListarAnunciosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarAnunciosPage),
  ],
})
export class ListarAnunciosPageModule {}
