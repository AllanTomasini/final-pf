import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AnuncioProvider } from "../../../../providers/anuncio/anuncio";

@IonicPage()
@Component({
  selector: "page-cadastrar-anuncio",
  templateUrl: "cadastrar-anuncio.html"
})
export class CadastrarAnuncioPage {
  private formAnuncio: FormGroup;
  private dataCriacao = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public anuncioProvider: AnuncioProvider
  ) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formAnuncio = this.formBuilder.group({
      titulo: ["", Validators.required],
      preco: ["", Validators.required],
      descricao: ["", Validators.required],
      dataCadastro: [],
      percentual: ["", Validators.required]
    });
  }

  adicionarAnuncio() {
    let formValues = this.formAnuncio.getRawValue();
    console.log(formValues);

    let { titulo, preco, descricao, dataCadastro, percentual } = formValues;
    let body = {
      titulo,
      preco,
      descricao,
      dataCadastro,
      percentual
    };
    body.dataCadastro = this.dataCriacao;

    console.log("body", body);

    this.anuncioProvider.adicionar(body).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );

    this.navCtrl.pop();
  }
}
