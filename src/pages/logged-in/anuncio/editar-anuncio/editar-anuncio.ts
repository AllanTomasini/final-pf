import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AnuncioProvider} from "../../../../providers/anuncio/anuncio";
import {Anuncio} from "../../../../models/anuncio";


@IonicPage()
@Component({
  selector: 'page-editar-anuncio',
  templateUrl: 'editar-anuncio.html',
})
export class EditarAnuncioPage {

  item: Anuncio = this.navParams.get('anuncio');
   private formAnuncio: FormGroup;
  item2 : Anuncio;
  id : string = this.navParams.get('id');
  private dataCriacao = new Date().toISOString();



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public anuncioProvider : AnuncioProvider
  ) {
  }
  ionViewDidLoad() {

  }
  ngOnInit() {
    this.initForm();
  }


  private initForm() {
    this.item2   = this.navParams.get('anuncio');
    this.formAnuncio = this.formBuilder.group({
      titulo: [this.item2.titulo, Validators.required],
      preco: [this.item2.preco, Validators.required],
      descricao: [this.item2.descricao, Validators.required],
      dataCadastro: [this.item2.dataCadastro],

    });


  }




  salvarSessao() {
    let formValues = this.formAnuncio.getRawValue();
    console.log(formValues);

    let {titulo, preco, descricao, dataCadastro} = formValues;
    let body = {
      titulo,
      preco,
      descricao,
      dataCadastro

    };
    body.dataCadastro = this.dataCriacao;

    this.anuncioProvider.editar(body, this.id).subscribe(response => {
        console.log(response)
        this.navCtrl.pop();
      },
      error => {console.log(error)}
    );
  }

}
