import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarPicosMutiraoPage } from './listar-picos-mutirao';

@NgModule({
  declarations: [
    ListarPicosMutiraoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarPicosMutiraoPage),
  ],
})
export class ListarPicosMutiraoPageModule {}
