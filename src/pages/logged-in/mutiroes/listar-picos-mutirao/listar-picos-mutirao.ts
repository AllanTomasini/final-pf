import { Component } from "@angular/core";
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  Platform
} from "ionic-angular";
import { Pico } from "../../../../models/pico";
import { PicoProvider } from "../../../../providers/pico/pico.provider";
@IonicPage()
@Component({
  selector: "page-listar-picos-mutirao",
  templateUrl: "listar-picos-mutirao.html"
})
export class ListarPicosMutiraoPage {
  picos: Pico[];
  picosAux: Pico[];
  loader: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public picoProvider: PicoProvider,
    public platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad ListarPicosMutiraoPage");
  }

  ngOnInit() {
    this.loader = this.presentLoading();
    this.loadData();
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }
  loadData() {
    this.picoProvider
      .listar()
      .toPromise()
      .then(response => {
        console.log(response);
        this.picos = response;
        this.picosAux = this.picos;
      })
      .then(() => this.loader.dismiss())
      .catch(error => {
        console.error(error);
        this.loader.dismiss();
      });
  }
  filtrar(event: any) {
    const value = event.target.value;

    if (value && value.trim() !== "" && value.length >= 3) {
      this.picos = this.picos.filter(
        pico => pico.nome.toLowerCase().indexOf(value.toLowerCase()) > -1
      );
    } else {
      this.picos = [...this.picosAux];
    }
  }
  goToParticipantes(id: number) {
    let p: Pico = this.picos.filter(q => q.id === id)[0];

    this.navCtrl.push("AdicionarParticipantePage", { pico: p });
  }
}
