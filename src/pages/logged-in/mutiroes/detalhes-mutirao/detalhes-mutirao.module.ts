import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesMutiraoPage } from './detalhes-mutirao';

@NgModule({
  declarations: [
    DetalhesMutiraoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesMutiraoPage),
  ],
})
export class DetalhesMutiraoPageModule {}
