import { Component } from "@angular/core";
import { AlertController, IonicPage, NavController, NavParams } from "ionic-angular";
import { Mutirao } from "../../../../models/mutirao";
import { Usuario } from "../../../../models/usuario";
import { MutiraoProvider } from "../../../../providers/mutirao/mutirao";

@IonicPage()
@Component({
  selector: "page-detalhes-mutirao",
  templateUrl: "detalhes-mutirao.html"
})
export class DetalhesMutiraoPage {
  item: Mutirao;
  id: string = this.navParams.get("id");
  _isParticipante;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public mutiraoProvider: MutiraoProvider,
    public alertCtrl: AlertController
  ) {}
  ionViewDidEnter() {
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
    this.carregarDetalhes();
  }
  carregarDetalhes() {
    let id = this.navParams.get("id");
    console.log(id);
    this.mutiraoProvider.findById(id).subscribe(
      response => {
        this.item = response;
        console.log(response);
      },
      error => {}
    );
  }
  participar() {
    this.mutiraoProvider.participar(this.id).subscribe(_response => {
      this.carregarDetalhes();
    });
  }

  sair() {
    this.mutiraoProvider.sair(this.id).subscribe(_response => {
      this.carregarDetalhes();
    });
  }

  get participante() {
    return (
      this.item &&
      this.item.participantes.find(u => u.nome === "Allan Tomasini")
    );
  }
  editarMutirao(mutirao: Mutirao, id: string) {
    this.navCtrl.push("EditarMutiraoPage", { mutirao: mutirao, id: id });
  }

  excluirMutirao() {
    let alert = this.alertCtrl.create({
      title: "Confirmar exclusão do Mutirão",
      message: "Você realmente deseja excluir esse Mutirão?",
      buttons: [
        {
          text: "Não",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Sim",
          handler: () => {
            this.mutiraoProvider.deleteById(this.id).subscribe(
              response => {
                console.log(response);
                this.navCtrl.pop();
              },
              error => {}
            );
          }
        }
      ]
    });
    alert.present();
  }

  goToParticipantes(participantes: Usuario[]) {
    this.navCtrl.push("ListaParticipantesPage", { id: this.id });
  }
}
