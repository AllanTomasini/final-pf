import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MutiraoProvider } from "../../../../providers/mutirao/mutirao";

@IonicPage()
@Component({
  selector: 'page-cadastrar-mutirao',
  templateUrl: 'cadastrar-mutirao.html',
})
export class CadastrarMutiraoPage {
  private formMutirao : FormGroup;
  private dataCriacao = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public mutiraoProvider : MutiraoProvider

  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formMutirao = this.formBuilder.group({

      dataMutirao : [,Validators.required],
      dataCadastro: [],

    });

  }

  cadastrarMutirao() {
    let formValues = this.formMutirao.getRawValue();
    console.log(formValues);
    let picoSelecionado = this.navParams.get("pico");
    let usuariosSelecionados = this.navParams.get("usuarios");

    let { dataMutirao , dataCadastro} = formValues;
    let body = {
      dataMutirao,
      dataCadastro,
      participantes: usuariosSelecionados,
      pico: picoSelecionado
    };
    body.dataCadastro = this.dataCriacao;
       console.log("body", body);

    this.mutiraoProvider.adicionar(body).subscribe(response => {
        console.log(response)
      },
      error => {console.log(error)}
    );

    this.navCtrl.popToRoot();
  }

}
