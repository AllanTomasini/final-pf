import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastrarMutiraoPage } from './cadastrar-mutirao';

@NgModule({
  declarations: [
    CadastrarMutiraoPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastrarMutiraoPage),
  ],
})
export class CadastrarMutiraoPageModule {}
