import { Component } from "@angular/core";
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams
} from "ionic-angular";
import { Mutirao } from "../../../../models/mutirao";
import { MutiraoProvider } from "../../../../providers/mutirao/mutirao";

@IonicPage()
@Component({
  selector: "page-listar-mutirao",
  templateUrl: "listar-mutirao.html"
})
export class ListarMutiraoPage {
  mutiroes: Mutirao[];
  mutiroesAux: Mutirao[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public mutiraoProvider: MutiraoProvider
  ) {}

  ionViewDidEnter() {
    this.loadData();
  }

  loadData() {
    let loader = this.presentLoading();
    this.mutiraoProvider
      .listar()
      .toPromise()
      .then(
        response => {

          this.mutiroes = response;
          this.mutiroesAux = this.mutiroes;
          loader.dismiss();
          console.log(response);
        },
        error => {
          loader.dismiss();
        }
      );
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  getDateString(mutirao: Mutirao){
    let dataMutirao:Date = mutirao.dataMutirao as Date;
    return `${dataMutirao.getDate()} - ${dataMutirao.getTime()}`;
  }
  goToAdicionarMutirao() {
    this.navCtrl.push("ListarPicosMutiraoPage");
  }

  goToDetalhe(id: string) {
    console.log(id);
    this.navCtrl.push("DetalhesMutiraoPage", { id: id });
  }

  filtrar(event: any) {
    const value = event.target.value;

    if (value && value.trim() !== "" && value.length >= 3) {
      this.mutiroes = this.mutiroes.filter(
        mutirao =>
          mutirao.pico.nome.toLowerCase().indexOf(value.toLowerCase()) > -1
      );
    } else {
      this.mutiroes = [...this.mutiroesAux];
    }
  }
}
