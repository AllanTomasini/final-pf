import { map } from "rxjs/operators";
import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormGroup } from "@angular/forms";
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams
} from "ionic-angular";
import { MutiraoProvider } from "../../../../providers/mutirao/mutirao";
import { Usuario } from "./../../../../models/usuario";
@IonicPage()
@Component({
  selector: "page-adicionar-participante",
  templateUrl: "adicionar-participante.html"
})
export class AdicionarParticipantePage {
  usuarios: Usuario[];
  usuariosAux: Usuario[];
  formUsuario: FormGroup;
  private _users;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public mutiraoProvider: MutiraoProvider,
    public fb: FormBuilder
  ) {}

  ionViewWillEnter() {
    this.initForm();
    this.loadData();
  }
  loadData() {
    let loader = this.presentLoading();
    this.mutiraoProvider
      .listarUsuarios()
      .toPromise()
      .then(
        response => {
          this.usuarios = response;
          this.usuariosAux = this.usuarios;
          loader.dismiss();
          this.buildForm();
          console.log(response);
        },
        error => {
          loader.dismiss();
        }
      );
  }

  initForm() {
    this.formUsuario = this.fb.group({
      users: this.fb.array([])
    });
  }

  buildForm() {
    let form = this.formUsuario.get("users") as FormArray;
    this.usuarios.forEach(user => {
      form.push(this.fb.control(false));
    });
  }

  get users() {
    return this.formUsuario.get("users");
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }
  filtrar(event: any) {
    const value = event.target.value;

    if (value && value.trim() !== "" && value.length >= 3) {
      this.usuarios = this.usuarios.filter(
        usuario => usuario.nome.toLowerCase().indexOf(value.toLowerCase()) > -1
      );
    } else {
      this.usuarios = [...this.usuariosAux];
    }
    this.initForm()
    this.buildForm();
  }

  goToCadastrar(form) {
    console.log(form);
    let value;

    let picoSelecionado = this.navParams.get("pico");

    const formValue = Object.assign({}, value, {
      users: form.users.map((selected, i) => {
        return {
          id: this.usuarios[i].id,
          nome: this.usuarios[i].nome,
          selected
        };
      })
    });

    let usuariosSelecionados = formValue.users
      .filter(u => u.selected)
      .map(user => {
        return { id: user.id, nome: user.nome };
      });

    console.log("afsdfa");
    console.log(usuariosSelecionados);

    this.navCtrl.push("CadastrarMutiraoPage", { pico: picoSelecionado, usuarios: usuariosSelecionados });
  }
}
