import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaParticipantesPage } from './lista-participantes';

@NgModule({
  declarations: [
    ListaParticipantesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaParticipantesPage),
  ],
})
export class ListaParticipantesPageModule {}
