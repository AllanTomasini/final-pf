import { Usuario } from './../../../../models/usuario';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { MutiraoProvider } from "../../../../providers/mutirao/mutirao";

/**
 * Generated class for the ListaParticipantesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-lista-participantes",
  templateUrl: "lista-participantes.html"
})
export class ListaParticipantesPage {

  participantes: Usuario[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public mutiraoProvider: MutiraoProvider
  ) {}

  ionViewDidEnter() {
    console.log("ionViewDidLoad ListaParticipantesPage");
    this.carregarDetalhes();
  }
  carregarDetalhes() {
    let id = this.navParams.get("id");
    console.log(id);
    this.mutiraoProvider.findById(id).subscribe(
      response => {
        this.participantes = response.participantes;
        console.log(response);
      },
      error => {}
    );
  }
}
