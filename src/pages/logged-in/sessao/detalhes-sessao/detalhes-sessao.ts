import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Sessao} from "../../../../models/sessao";
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {Pico} from "../../../../models/pico";



@IonicPage()
@Component({
  selector: 'page-detalhes-sessao',
  templateUrl: 'detalhes-sessao.html',
})
export class DetalhesSessaoPage {
  item: Sessao;
  id : string = this.navParams.get('id');
  pico = {
    id: 1,
    nome : 'Jardim Ambiental',
    descricao : 'transição',
    entrada : 0 ,
    dataCriacao : new Date()
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sessaoProvider : SessaoProvider,
    public alertCtrl: AlertController
  ) {
  }

  ionViewDidEnter(){
    this.carregarDetalhes();
  }

  ionViewDidLoad() {
    this.carregarDetalhes();
  }
  carregarDetalhes(){
    let id = this.navParams.get('id');
    console.log(id);
    this.sessaoProvider.findById(id)
      .subscribe(response => {
          this.item = response;
          console.log(response);
        },
        error => {});
  }

  editarSessao( sessao : Sessao, id : string) {
    this.navCtrl.push("EditarSessaoPage", {sessao : sessao, id : id});
  }

  excluirSessao() {
    console.log('teste');
      let alert = this.alertCtrl.create({
        title: 'Confirmar exclusão de sessão',
        message: 'Você realmente deseja excluir essa sessão?',
        buttons: [
          {
            text: 'Não',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Sim',
            handler: () => {
              this.sessaoProvider.deleteById(this.id).subscribe(response => {
                  this.item = response;
                  console.log(response);
                  this.navCtrl.pop();
                },
                error => {});
            }
          }
        ]
      })
  alert.present();
  }
}
