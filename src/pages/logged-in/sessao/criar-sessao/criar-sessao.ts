import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SessaoProvider} from "../../../../providers/sessao/sessao";

/**
 * Generated class for the CriarSessaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-sessao',
  templateUrl: 'criar-sessao.html',
})
export class CriarSessaoPage {

  private formSessao: FormGroup;
  private dataCriacao = new Date().toISOString();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public sessaoProvider : SessaoProvider

  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formSessao = this.formBuilder.group({

      dataSessao : [,Validators.required],
      dataCadastro: [],

    });

  }

  adicionarSessao() {
    let formValues = this.formSessao.getRawValue();
    console.log(formValues);

    let { dataSessao, dataCadastro} = formValues;
    let body = {

      dataSessao,
      dataCadastro
    };
    body.dataCadastro = this.dataCriacao;
    // this.dataAux = body.dataSessao;
    // body.dataSessao = new Date(Date.parse(Date()));
    // body.dataSessao = this.dataAux;
    // body.dataSessao.toDateString();
    console.log("body", body);

    this.sessaoProvider.adicionar(body).subscribe(response => {
        console.log(response)
      },
      error => {console.log(error)}
    );

    this.navCtrl.pop();
  }

}
