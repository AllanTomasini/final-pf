import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {SessaoProvider} from "../../../../providers/sessao/sessao";
import {Sessao} from "../../../../models/sessao";
import {Pico} from "../../../../models/pico";

/**
 * Generated class for the ListarSessaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listar-sessao',
  templateUrl: 'listar-sessao.html',
})
export class ListarSessaoPage {
  items : Sessao [] ;
  pico = {
    id:1,
    nome : 'Jardim Ambiental',
    descricao : 'transição',
    entrada : 0 ,
    dataCriacao : new Date()
  };


  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl : LoadingController,
              public sessaoProvider : SessaoProvider) {

  }

  ionDidLoad() {
    this.loadData();
  }
  ionViewDidEnter() {
    this.loadData();
  }

  loadData() {

    let loader = this.presentLoading();
    this.sessaoProvider.listar()
      .subscribe(response => {
          this.items = response;
              loader.dismiss();
          console.log(response);

        },
        error => {
          loader.dismiss();
        });
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  cadastrarSessao() {
    this.navCtrl.push("CriarSessaoPage");
  }

  detalheSessao(id: string) {
    this.navCtrl.push("DetalhesSessaoPage", {id:id});
  }
}
