import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarSessaoPage } from './listar-sessao';

@NgModule({
  declarations: [
    ListarSessaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarSessaoPage),
  ],
})
export class ListarSessaoPageModule {}
