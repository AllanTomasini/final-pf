import { SafeResourceUrl, DomSanitizer } from "@angular/platform-browser";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";

/**
 * Generated class for the VideosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-videos",
  templateUrl: "videos.html"
})
export class VideosPage {
  url: SafeResourceUrl;
  videos: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private domSanitizer: DomSanitizer,
    public picoProvider: PicoProvider
  ) {}

  ionViewWillEnter() {
    this.loadData();
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad VideosPage");
  }

  loadData() {
    let id = this.navParams.get("id");
    console.log(id);
    this.picoProvider.findById(id).toPromise().then(
      response => {
        this.videos = response.videos.map(video => {
          return { ...video, url: this.getSanitizedUrl(video.urlYoutube) };
        });
        console.log(response);
      },
      error => {}
    );
  }

  getSanitizedUrl(url: string): SafeResourceUrl {
    let tentativaUm = url.split("watch?v=");

    if (tentativaUm.length > 1) {
      let re = "https://www.youtube.com/embed/" + tentativaUm[1].split("&")[0];
      console.log(re);
      return this.domSanitizer.bypassSecurityTrustResourceUrl(re);
    }

    let tentativaDois = url.split(".be/");

    if (tentativaDois.length > 1) {
      let re = "https://www.youtube.com/embed/" + tentativaDois[1];
      console.log(re);
      return this.domSanitizer.bypassSecurityTrustResourceUrl(re);
    }
  }
}
