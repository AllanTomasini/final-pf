import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";
import { HttpConfigProvider } from "./../../../../providers/http-config/http-config";

@IonicPage()
@Component({
  selector: "page-adicionar-video",
  templateUrl: "adicionar-video.html"
})
export class AdicionarVideoPage {
  formVideo: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public picoProvider: PicoProvider,
    public http: HttpConfigProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad AdicionarVideoPage");
    this.initForm();
  }

  private initForm() {
    this.formVideo = this.formBuilder.group({
      descricao: ["", Validators.required],
      urlYoutube: ["", Validators.required]
    });
  }
  public adicionar() {
    let formValues = this.formVideo.getRawValue();
    console.log(formValues);
    let idPico = this.navParams.get('id');

    let { descricao, urlYoutube } = formValues;
    let body = {
      descricao,
      urlYoutube,
      idPico
    };

    console.log("body", body);
    this.picoProvider.adicionarVideo(body).toPromise().then(
      test => console.log(test),
      error => {
        console.log(error);
      }
    );

    this.navCtrl.pop();
  }
}
