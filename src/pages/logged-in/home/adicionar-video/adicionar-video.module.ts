import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarVideoPage } from './adicionar-video';

@NgModule({
  declarations: [
    AdicionarVideoPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarVideoPage),
  ],
})
export class AdicionarVideoPageModule {}
