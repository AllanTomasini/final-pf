import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Geolocation } from "@ionic-native/geolocation";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";
import { HttpConfigProvider } from "./../../../../providers/http-config/http-config";

@IonicPage()
@Component({
  selector: "page-adicionar-pico",
  templateUrl: "adicionar-pico.html"
})
export class AdicionarPicoPage {
  formPico: FormGroup;
  private localizacao;
  private foto;
  private type: any;
  private fileName: string;
  private arquivo: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public picoProvider: PicoProvider,
    public geolocation: Geolocation,
    public http: HttpConfigProvider,
  ) {}

  ngOnInit() {
    this.initForm();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AdicionarPicoPage");
  }

  private initForm() {
    this.formPico = this.formBuilder.group({
      nome: ["", Validators.required],
      descricao: ["", Validators.required],
      endereco: ["", Validators.required],
      numero: ["", Validators.required],
      entrada: [""],
      gratuito: [""],
      capa: [null, Validators.required]
    });
    this.geolocation.getCurrentPosition().then(position => {
      this.localizacao = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      };
    });
  }

  public adicionar() {
    let formValues = this.formPico.getRawValue();
    console.log(formValues);

    let { nome, descricao, entrada, gratuito, numero, endereco } = formValues;
    let body = {
      nome,
      descricao,
      entrada,
      gratuito,
      endereco: { rua: endereco, numero, localizacao: this.localizacao },
      urlCapa: this.fileName
    };

    console.log("body", body);
    this.picoProvider.adicionar(body).subscribe(
      test => console.log(test),
      error => {
        console.log(error);
      }
    );

    this.readFile(this.formPico.get("file"));
    this.navCtrl.pop();
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const fileList: FileList = event.target.files;
      if (fileList.length > 0) {
        const file = fileList[0];

        const formData = new FormData();
        formData.append("file", file, file.name);
        this.fileName = file.name;

        this.arquivo = formData;
      }
    }
  }
  private readFile(file: any) {
    this.http.postData(this.arquivo);
  }
}
