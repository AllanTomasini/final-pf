import { Component } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation";
import {
  LaunchNavigator,
  LaunchNavigatorOptions
} from "@ionic-native/launch-navigator";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { Pico } from "../../../../models/pico";
import { PicoProvider } from "../../../../providers/pico/pico.provider";
import { Localizacao } from "./../../../../models/localizacao";

@IonicPage()
@Component({
  selector: "page-detalhe-pico",
  templateUrl: "detalhe-pico.html"
})
export class DetalhePicoPage {
  item: Pico;
  localizacao: Localizacao;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public picoProvider: PicoProvider,
    public geolocation: Geolocation,
    private launchNavigator: LaunchNavigator,
    public platform: Platform
  ) {}

  ionViewWillEnter() {
        this.loadData();
    this.geolocation
      .getCurrentPosition()
      .then(position => {
        this.localizacao = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        };

        this.loadData();
      })
      .catch(error => {
        console.log(error);
        this.loadData();
      });
  }
  loadData() {
    let id = this.navParams.get("id");
    console.log(id);
    this.picoProvider.findById(id).toPromise().then(
      response => {
        this.item = response;
        console.log(response);
      },
      error => {}
    );
  }

  navegar(latitudeDestino: number, longitudeDestino) {
    let { latitude, longitude } = this.localizacao;
    let inicio = latitude + "," + longitude;

    let destino = latitudeDestino + "," + longitudeDestino;
    console.log(destino);
    window.open(
      "https://www.google.com/maps/dir/?api=1&origin=" +
        inicio +
        "&destination=" +
        destino,
      "_system",
      "location=yes"
    );
  }

  editarFavorito() {
    let id = this.navParams.get("id");
    console.log(id);
    this.picoProvider.editarFavorito(id).subscribe(
      () => {
        this.loadData();
      },
      error => {}
    );
  }

  goToComentarios(id: string) {
    this.navCtrl.push("ComentariosPage", { id: id });
  }

  goToVideos(id: number) {
    this.navCtrl.push("VideosPage", { id: id });
  }

  goToAddVideos(id: number) {
    this.navCtrl.push("AdicionarVideoPage", { id: id });
  }

  goToAddFotos(id: number) {
    this.navCtrl.push('AdicionarFotosPage', {id:id});
  }

  goToFotos(id: number) {
    this.navCtrl.push("FotosPage", { id: id });
  }

  goToAddDenuncia(id: number) {
    this.navCtrl.push("AdicionarDenunciaPage", { id: id });
  }
}
