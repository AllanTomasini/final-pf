import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";
import { Pico } from "./../../../../models/pico";

@IonicPage()
@Component({
  selector: "page-comentarios",
  templateUrl: "comentarios.html"
})
export class ComentariosPage {
  pico: Pico;
  formComentario: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public picoProvider: PicoProvider,
    public formBuilder: FormBuilder
  ) {}
  ngOnInit() {
    this.initForm();
  }

  ionViewDidLoad() {
    this.loadData();
  }
  private initForm() {
    this.formComentario = this.formBuilder.group({
      comentario: ["", Validators.required]
    });
  }
  loadData() {
    let id = this.navParams.get("id");
    console.log(id);
    this.picoProvider.findById(id).subscribe(
      response => {
        this.pico = response;
        console.log(response);
      },
      error => {}
    );
  }

  adicionarComentario() {
    let id = this.navParams.get("id");
    let texto = this.formComentario.get("comentario").value;

    this.picoProvider
      .comentar(id, { comentario: texto })
      .toPromise()
      .then(() => {
        this.loadData();
        this.formComentario.get("comentario").patchValue("");
      })
      .catch(error => {
        console.log(error);
      });
  }
}
