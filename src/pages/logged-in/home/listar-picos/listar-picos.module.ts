import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarPicosPage } from './listar-picos';

@NgModule({
  declarations: [
    ListarPicosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarPicosPage),
  ],
})
export class ListarPicosPageModule {}
