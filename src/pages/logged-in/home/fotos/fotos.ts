import { Foto } from "./../../../../models/foto";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";

@IonicPage()
@Component({
  selector: "page-fotos",
  templateUrl: "fotos.html"
})
export class FotosPage {
  fotos: Foto[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public picoProvider: PicoProvider
  ) {}

  ionViewWillEnter() {
    this.loadData();
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad VideosPage");
  }

  loadData() {
    let id = this.navParams.get("id");
    console.log(id);
    this.picoProvider
      .findById(id)
      .toPromise()
      .then(
        response => {
          this.fotos = response.fotos;
          console.log(response);
        },
        error => {}
      );
  }
}
