import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarFotosPage } from './adicionar-fotos';

@NgModule({
  declarations: [
    AdicionarFotosPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarFotosPage),
  ],
})
export class AdicionarFotosPageModule {}
