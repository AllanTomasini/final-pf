import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PicoProvider } from "../../../../providers/pico/pico.provider";
import { HttpConfigProvider } from "../../../../providers/http-config/http-config";

@IonicPage()
@Component({
  selector: "page-adicionar-fotos",
  templateUrl: "adicionar-fotos.html"
})
export class AdicionarFotosPage {
  formFoto: FormGroup;
  private fileName: string;
  private arquivo: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public picoProvider: PicoProvider,
    public http: HttpConfigProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad AdicionarVideoPage");
    this.initForm();
  }

  private initForm() {
    this.formFoto = this.formBuilder.group({
      descricao: ["", Validators.required]
    });
  }
  public adicionar() {
    let formValues = this.formFoto.getRawValue();
    console.log(formValues);
    let idPico = this.navParams.get("id");

    let { descricao, path } = formValues;
    let body = {
      descricao,
      path: this.fileName,
      idPico
    };

    console.log("body", body);
    this.picoProvider
      .adicionarFoto(body)
      .toPromise()
      .then(
        test => console.log(test),
        error => {
          console.log(error);
        }
      );

    this.readFile(this.formFoto.get("file"));
    this.navCtrl.pop();
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const fileList: FileList = event.target.files;
      if (fileList.length > 0) {
        const file = fileList[0];

        const formData = new FormData();
        formData.append("file", file, file.name);
        this.fileName = file.name;

        this.arquivo = formData;
      }
    }
  }
  private readFile(file: any) {
    this.http.postData(this.arquivo);
  }
}
