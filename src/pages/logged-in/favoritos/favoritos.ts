import { Component, OnInit } from "@angular/core";
import { LaunchNavigator, LaunchNavigatorOptions } from "@ionic-native/launch-navigator";
import { IonicPage, LoadingController, NavController, NavParams, Platform } from "ionic-angular";
import { PicoProvider } from "../../../providers/pico/pico.provider";
import { Localizacao } from "./../../../models/localizacao";
import { Pico } from "./../../../models/pico";
import { Geolocation } from "@ionic-native/geolocation";

@IonicPage()
@Component({
  selector: "page-favoritos",
  templateUrl: "favoritos.html"
})
export class FavoritosPage {
  picos: Pico[];
  localizacao: Localizacao;
  picosAux: Pico[];
  loader: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public picoProvider: PicoProvider,
    public geolocation: Geolocation,
    private launchNavigator: LaunchNavigator,
    public platform: Platform
  ) {}

  ionViewDidEnter() {
    this.geolocation
      .getCurrentPosition()
      .then(position => {
        this.loader = this.presentLoading();
        this.localizacao = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        };

        this.loadData();
      })
      .catch(error => {
        console.log(error);
        this.loadData();
      });
  }

  loadData() {
    this.picoProvider
      .listarFavoritos()
      .toPromise()
      .then(response => {
        console.log(response);
        this.picos = response;
        if (this.localizacao) {
          this.picos = this.picos.map(pico => {
            return {
              ...pico,
              distancia: this.getDistanceFromLatLonInKm(
                this.localizacao.latitude,
                this.localizacao.longitude,
                pico.endereco.localizacao.latitude,
                pico.endereco.localizacao.longitude
              )
            };
          });
          this.picos.sort((a, b) => {
            if (a.distancia < b.distancia) return -1;
            if (a.distancia > b.distancia) return 1;
            return 0;
          });
          console.log(this.picos);
          this.picosAux = this.picos;
        }
      })
      .then(() => this.loader.dismiss())
      .catch(error => {
        console.error(error);
        this.loader.dismiss();
      });
  }

  filtrar(event: any) {
    const value = event.target.value;

    if (value && value.trim() !== "" && value.length >= 3) {
      this.picos = this.picos.filter(
        pico => pico.nome.toLowerCase().indexOf(value.toLowerCase()) > -1
      );
    } else {
      this.picos = [...this.picosAux];
    }
  }

  navegar(latitudeDestino: number, longitudeDestino) {
    let { latitude, longitude } = this.localizacao;
    let inicio = latitude + "," + longitude;

    if (this.platform.is("android")) {
      let options: LaunchNavigatorOptions = {
        start: inicio
      };

      this.launchNavigator.navigate(
        [latitudeDestino, longitudeDestino],
        options
      );
    } else {
      let destino = latitudeDestino + "," + longitudeDestino;
      console.log(destino);
      window.open(
        "https://www.google.com/maps/dir/?api=1&origin=" +
          inicio +
          "&destination=" +
          destino,
        "_system",
        "location=yes"
      );
    }
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();
    return loader;
  }

  goToDetalhe(id: string) {
    this.navCtrl.push("DetalhePicoPage", { id: id });
  }

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) *
        Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

  editarFavorito(id: string) {
    console.log(id);
    this.picoProvider.editarFavorito(id).subscribe(
      () => {
        this.loadData();
      },
      error => {}
    );
  }
}
