import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {EventoProvider} from "../../../../providers/evento/evento";
import {Evento} from "../../../../models/evento";



@IonicPage()
@Component({
  selector: 'page-detalhes-evento',
  templateUrl: 'detalhes-evento.html',
})
export class DetalhesEventoPage {

  item: Evento;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public eventoProvider: EventoProvider    ) {
  }

  ionViewDidLoad() {
    let id = this.navParams.get('id');
    console.log(id);
    this.eventoProvider.findById(id)
      .subscribe(response => {
          this.item = response;
          console.log(response);
        },
        error => {});
  }

}
