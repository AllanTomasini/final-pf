import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComentariosEventosPage } from './comentarios-eventos';

@NgModule({
  declarations: [
    ComentariosEventosPage,
  ],
  imports: [
    IonicPageModule.forChild(ComentariosEventosPage),
  ],
})
export class ComentariosEventosPageModule {}
